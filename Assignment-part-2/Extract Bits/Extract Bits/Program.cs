﻿// Extract Bits
//To extract the given bits from start to stop point and display the decimal value

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extract_Bits
{
    internal class Program
    {
        static void Main(string[] args)
        {
        StartProgram:
            try
            {
                while (true)
                {
                    
                    Console.Write("Enter the Input number: ");
                    int InputNumber = Convert.ToInt32(Console.ReadLine()); // To get input decimal value
                    String BinaryNumber = Convert.ToString(InputNumber, 2); // Conversion from decimal to binary
                    Console.WriteLine($"The Binary Form of the Input number is: {BinaryNumber}"); // TO print the binary form
                    BitsInput:
                    Console.Write("\nEnter the Start bit: ");
                    int StartBit = Convert.ToInt32(Console.ReadLine()); // To get the Start bit which is starting from the LSB

                    Console.Write("\nEnter the End bit: ");
                    int EndBit = Convert.ToInt32(Console.ReadLine()); // To get the End bit which is starting from the LSB

                    Console.WriteLine();

                    ///<summary>
                    /// To reverse and store the binary to print the Required bits hence it has to be taken from reverse direction but must be printed in original order
                    /// </summary>

                   
                    if (EndBit < BinaryNumber.Length)
                    {
                         String BinaryRev = "";

                        for (int index = BinaryNumber.Length - 1; index >= 0; index--)
                        {
                            BinaryRev = BinaryRev + BinaryNumber[index]; // Stores reversed binary number in BinaryRev
                        }
                        if (EndBit > StartBit)  // Condition to check whether End bit is greater than start bit
                        {
                            Console.WriteLine("-----------------------------");
                            Console.Write("Extracted Binary Field value: ");
                            for (int index = EndBit; index >= StartBit; index--) // To print the require bits in correct required order
                            {
                                Console.Write(BinaryRev[index]);
                            }
                            Console.WriteLine();
                            Console.WriteLine("------------------------------");
                            Console.WriteLine();

                            ///<summary>
                            /// Logic to converted the extraced bits into decimal value
                            /// </summary>
                            int DecimalValue = 0;
                            for (int index = EndBit; index >= StartBit; index--)
                            {
                                if (BinaryRev[index] == '1')
                                {
                                    DecimalValue = (DecimalValue * 2) + 1;
                                }
                                else
                                {
                                    DecimalValue = (DecimalValue * 2);
                                }
                            }
                            Console.WriteLine($"The Decimal Field value is :{DecimalValue}");
                            Console.WriteLine("-----------------------------------");
                        }
                        else
                        {
                            Console.WriteLine(" Enter Start bit less than End bit");
                            goto BitsInput;  // If end bit is small than start bit asks for the input again
                        }
                    }
                    else
                    {
                        Console.WriteLine("Index out of range enter proper index ");
                        goto BitsInput;
                    }

                    

                    Console.WriteLine("Do you want to continue the program y/n: "); 
                    char Answer = Convert.ToChar(Console.ReadLine());

                    if ((Answer == 'y') || (Answer == 'Y')) {  // To check if the user wants to continue the program else it will break
                        Console.WriteLine("---------------------------------------");
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                
                

            }
            catch (Exception ex)
            {
                Console.WriteLine("--------------------------");
                Console.WriteLine(ex.Message); 
                Console.WriteLine("--------------------------");
                goto StartProgram;
            }
            

            Console.ReadKey();

        }
    }
}
