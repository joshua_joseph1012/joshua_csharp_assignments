﻿// Case conversion to upper string

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Case_conversion
{
    internal class Program
    {
        static void Main(string[] args)
        {
            while (true) // Program runs untill stop is enterd
            {
                Console.WriteLine("Enter the string to be converted into upper case: ");
                String InputString = Console.ReadLine();
                if (InputString.ToLower() == "stop") // To check whether the input string is stop 
                {
                    break; // If it is stop break the execution
                }
                else
                {
                    Console.WriteLine("-----------------------------");
                    Console.WriteLine("Case converted string is {0}",InputString.ToUpper()); // To convert the string to upper case
                    Console.WriteLine("-----------------------------");
                }
                
            }
            Console.ReadKey();



        }
    }
}
