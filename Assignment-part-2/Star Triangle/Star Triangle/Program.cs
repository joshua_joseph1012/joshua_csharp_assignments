﻿// Print * Triangle

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Triangle
{
    internal class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                StartProgram:
                Console.WriteLine("\nEnter the Number less than or equal to 10 to create a * trangle");
                int NumberOfRows = Convert.ToInt32(Console.ReadLine()); // TO ge the input number
                if (NumberOfRows <= 10) // To check if the input is less than 10
                {
                    for (int index1 = 0; index1 < NumberOfRows; index1++)
                    {
                        for (int index2 = 0; index2 < NumberOfRows - index1; index2++)
                        {
                            Console.Write(" "); // To print empty spaces
                        }
                        for (int index3 = 0; index3 < (index1 + 1); index3++)
                        {
                            Console.Write("* "); // To print star in the form of triangle
                        }
                        Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine("\nValue is not in range");
                    goto StartProgram;  // If the vlaue is greater than 10 goto StartProgam

                }
                
                Console.WriteLine("Do you need to continue the program y/n:");
                char Answer = Convert.ToChar(Console.ReadLine()); // To get input from user if they want to continue the program
                if((Answer =='y') || (Answer == 'y')){
                    continue;  // If yes continue
                }
                else
                {
                    break;  // If no break the loop
                }
            }
           

            Console.ReadKey();
        }

    }
}
