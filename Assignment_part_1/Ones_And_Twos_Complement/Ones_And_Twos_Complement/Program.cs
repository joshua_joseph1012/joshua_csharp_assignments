﻿// Assignment to print one's and two's compement of the given number

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ones_And_Twos_Complement
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Enter the number to find the 1's and 2's Complement"); 
            int InputNumber = Convert.ToInt32(Console.ReadLine()); // Reads the input number in integer format

            Console.WriteLine("The Binary representation of the InputNumber is: ");
            String BinaryNum = Convert.ToString(InputNumber,2); // Returns the binary representation of the Input number
            Console.WriteLine(BinaryNum); // Prints the binary number
            Console.WriteLine("\n");

            Console.Write("\nEnter the required Bits to Convert: "); 
            int Bits = Convert.ToInt32(Console.ReadLine()); // Reads the bits required to display the complement

            int Complement = ~InputNumber; // Operator used to give one's complement
            Console.WriteLine(Complement);
            String OnceComplement= Convert.ToString(Complement,2); // To convert the one's complement number to binary
            Console.WriteLine("\n-------Once Complement-------");
            String SubOnceComplement = OnceComplement.Substring(OnceComplement.Length -Bits); // To extract the required bits from the converted binary
            Console.WriteLine("The Binary representation of the 1's Complementis :{0}",SubOnceComplement); // To print the one's complement
            
            ///<summary>
            /// This is one of the method to find the decimal value from binary value and conditions for 0 and 1 in binary representation
            ///</summary>
            int DecimalValueOnes = 0;  
            for (int index = 0; index < Bits; index++) 
            {
                if(SubOnceComplement[index] == '1')
                {
                    DecimalValueOnes = (DecimalValueOnes * 2) + 1; 
                }
                else
                {
                    DecimalValueOnes = (DecimalValueOnes * 2); 
                }

            }
            Console.WriteLine($"Decimal value of 1's Complement {DecimalValueOnes}"); // Prints the decimal value of one's complement
            Console.WriteLine("\n");
            Console.WriteLine("-------Twos Complement-------");
            
            String TwosComplement= Convert.ToString(Complement+1,2); // Logic to convert one's complement into two's complement by adding one to one's complement
            String SubTwosComplement = TwosComplement.Substring(TwosComplement.Length -Bits); // To extract the required bits from the converted binary
            Console.WriteLine("Binary representation of 2's Comlement is: {0}",SubTwosComplement); // To print the two's complement
           
            int DecimalValueTwos = 0;
            for (int index = 0; index < Bits; index++)
            {
                if (SubTwosComplement[index] == '1')
                {
                    DecimalValueTwos = (DecimalValueTwos * 2) + 1;
                }
                else
                {
                    DecimalValueTwos = (DecimalValueTwos * 2);
                }

            }
            Console.WriteLine($"Decimal value of 2's Complement {DecimalValueTwos}\n"); // Prints the decimal value of two's complement

            Console.ReadKey();
        }
        
    }
}
