﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Area_and_Perimeter
{
    internal class Program
    { 

        static  void SquareAreaAndPerimeter()
        {
            ///<summary>
            /// Reads the sides and calculates the Area and Perimeter with the applied formula
            /// </summary>
            Console.Write("\nEnter the side of the Square: ");
            int Side = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("-------------------");
            Console.WriteLine("Area of the Square is: {0}\nPerimeter of the square is: {1}\n", Math.Pow(Side,2) , 4 * Side);
        }
        static void RectangleAreaAndPerimeter()
        {
            ///<summary>
            /// Reads the Length and Width and calculates the Area and Perimeter with the applied formula
            /// </summary>
            Console.Write("\nEnter the Length of the Rectangle: ");
            int length = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the Width of the Rectangle: ");
            int width = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("-------------------");
            Console.WriteLine("\nArea of the Rectangle is: {0}\nPerimeter of the Rectangle is: {1}\n", length * width, 2 * (length+width));
        }

        static void CircleAreaAndPerimeter()
        {
            ///<summary>
            /// Reads the radius and calculates the Area and Perimeter(circumference) with the applied formula
            /// </summary>
            const double pi = 3.14159; 
            Console.Write("\nEnter the Radius of the Circle: ");
            int radius = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("-------------------");
            Console.WriteLine("\nArea of the Circle is: {0}\nPerimeter(Circumference) of the Circle is: {1}\n", pi*(Math.Pow(radius,2)) , 2 * pi * radius);
        }

        static void TriangleAreaAndPerimeter()
        {
            ///<summary>
            /// Reads the sides, height, base  and calculates the Area and Perimeter(circumference) with the applied formula
            /// </summary>

            Console.Write("\nEnter the Height of the Triangle: ");
            int height = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the SideOne of the Triangle: ");
            int SideOne = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the SideTwo of the Triangle: ");
            int SideTwo = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the BaseSide of the Triangle: ");
            int BaseSide = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("-------------------");
            Console.WriteLine("\nArea of the Rectangle is: {0}\nPerimeter of the Rectangle is: {1}\n", (height*BaseSide)/2, SideOne+BaseSide+SideTwo);
        }


        static void Main(string[] args)
        {
            Console.WriteLine("-----------CALCULATE THE AREA AND PERIMETER---------\n");
            Console.WriteLine("Choose a option for the following menu");
            Console.WriteLine("\t-->A: Square\n\t-->B: Rectangle\n\t-->C: Circle\n\t-->D: Triangl"); // Displays the options to the user
            Console.Write("Enter an Option from above: ");
            
            char Shape = Convert.ToChar(Console.ReadLine()); // Reads the input character option
            
            ///<summary>
            /// Switch case to select the operation needed to be performed and the required function to be called.
            ///</summary>
       
            switch (Shape)
            {
                case 'A':
                    case 'a':
                    SquareAreaAndPerimeter();
                    break;
                case 'B':
                    case 'b':
                    RectangleAreaAndPerimeter();
                    break;
                case 'C':
                    case 'c':
                    CircleAreaAndPerimeter();
                    break;
                case 'D':
                    case'd':
                    TriangleAreaAndPerimeter();
                    break;
                default:
                    Console.WriteLine("Invalid Option");
                    break;

            }
            Console.ReadKey();
        }
    }
}
